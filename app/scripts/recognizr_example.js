function Gesture(pattern, callback) {
	this.callback = callback;
	this.pattern = pattern;
	this._points = new Array();
	this._r = new DollarRecognizer();

	this.canvas = document.getElementById('myCanvas');
	this._g = this.canvas.getContext('2d');
	this._g.fillStyle = "rgb(0,0,225)";
	this._g.strokeStyle = "rgb(0,0,225)";
//	this._g.lineWidth = 3;
	this._rc = this.getCanvasRect(); // canvas rect on page
	this._isDown = false;

	this._g.lineWidth = 2;
	this._g.lineJoin = this._g.lineCap = 'round';


	this.canvas.addEventListener('mousedown', this.mouseDownEvent.bind(this), false);
	this.canvas.addEventListener('mouseup', this.mouseUpEvent.bind(this), false);
	this.canvas.addEventListener('mousemove', this.mouseMoveEvent.bind(this), false);


	this.Bpoints = [];




	this.destroy = function() {
		this.canvas.removeEventListener('mousedown', this.mouseDownEvent.bind(this), false);
		this.canvas.removeEventListener('mouseup', this.mouseUpEvent.bind(this), false);
		this.canvas.removeEventListener('mousemove', this.mouseMoveEvent.bind(this), false);
	}

}

Gesture.prototype.mouseDownEvent = function(event) {
	var x = event.clientX,
			y = event.clientY;

	document.onselectstart = function() { return false; } // disable drag-select
	document.onmousedown = function() { return false; } // disable drag-select

	this.Bpoints = [ ];
	this.Bpoints.push({ x: event.clientX, y: event.clientY });

	this._isDown = true;
	x -= this._rc.x;
	y -= this._rc.y - this.getScrollY();
	if (this._points.length > 0)
		this._g.clearRect(0, 0, this._rc.width, this._rc.height);
	this._points.length = 1; // clear
	this._points[0] = new Point(x, y);
};

Gesture.prototype.mouseMoveEvent = function(event) {
	var x = event.clientX,
			y = event.clientY;



	if (this._isDown) {

		this.Bpoints.push({ x: event.clientX, y: event.clientY });

		x -= this._rc.x;
		y -= this._rc.y - this.getScrollY();
		this._points[this._points.length] = new Point(x, y); // append
		//this.drawConnectedPoint(this._points.length - 2, this._points.length - 1);

		this.Brush()

	}
};

Gesture.prototype.mouseUpEvent = function(event) {
	var x = event.clientX,
			y = event.clientY;

	document.onselectstart = function() { return true; } // enable drag-select
	document.onmousedown = function() { return true; } // enable drag-select

	if (!this._isDown)
		return false;

	this._isDown = false;

	this._g.clearRect(0, 0, this._rc.width, this._rc.height);

	if (this._points.length <= 10) {
		return false;
	}

	var result = this._r.Recognize(this._points, true);

	if (result.Name === this.pattern ) {
		this.callback(result);
	}


};

Gesture.prototype.getCanvasRect = function() {
	var w = this.canvas.width;
	var h = this.canvas.height;

	var cx = this.canvas.offsetLeft;
	var cy = this.canvas.offsetTop;

	while (this.canvas.offsetParent != null) {
		this.canvas = this.canvas.offsetParent;
		cx += this.canvas.offsetLeft;
		cy += this.canvas.offsetTop;
	}

	return {x: cx, y: cy, width: w, height: h};
}

Gesture.prototype.getScrollY = function() {
	var scrollY = 0;
	if (typeof(document.body.parentElement) != 'undefined') {
		scrollY = document.body.parentElement.scrollTop; // IE
	} else if (typeof(window.pageYOffset) != 'undefined') {
		scrollY = window.pageYOffset; // FF
	}

	return scrollY;
}

Gesture.prototype.drawConnectedPoint = function(from, to) {
	// this._g.beginPath();
	// this._g.moveTo(this._points[from].X, this._points[from].Y);
	// this._g.lineTo(this._points[to].X, this._points[to].Y);
	// this._g.closePath();
	// this._g.stroke();
}

Gesture.prototype.Brush = function() {

	this._g.beginPath();

	this._g.moveTo(this.Bpoints[this.Bpoints.length - 2].x, this.Bpoints[this.Bpoints.length - 2].y);
	this._g.lineTo(this.Bpoints[this.Bpoints.length - 1].x, this.Bpoints[this.Bpoints.length - 1].y);
	this._g.stroke();

	for (var i = 0, len = this.Bpoints.length; i < len; i++) {
		dx = this.Bpoints[i].x - this.Bpoints[this.Bpoints.length-1].x;
		dy = this.Bpoints[i].y - this.Bpoints[this.Bpoints.length-1].y;
		d = dx * dx + dy * dy;

		if (d < 1000) {
			this._g.beginPath();
			this._g.strokeStyle = 'rgba(0,0,0,0.3)';
			this._g.moveTo( this.Bpoints[this.Bpoints.length-1].x + (dx * 0.2), this.Bpoints[this.Bpoints.length-1].y + (dy * 0.2));
			this._g.lineTo( this.Bpoints[i].x - (dx * 0.2), this.Bpoints[i].y - (dy * 0.2));
			this._g.stroke();
		}
	}

}




function success(data) {
	console.log('we have success', data)
}

var gest = new Gesture('rectangle', success);

console.log(gest);