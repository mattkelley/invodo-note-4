

;(function ( App, undefined ) {


// Utils
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};


// Init
// (function(){
	// Get language by query parameter
	App.lang = getParameterByName('lang') || 'en';
	// Get App data
	App.data = data(App.lang);
	// Mustache
	var template = document.getElementById('template').innerHTML
	// Parse template
	Mustache.parse(template);
	// Insert template
	document.getElementById('all').innerHTML = Mustache.render(template, App.data);
	// Set language as body class
	document.body.className = App.lang;

// }).call(this);




var steps = document.getElementById('steps')

var step = steps.children;


App.nextStep = function() {
	var next = this.stepVal+1;
	// Turn on new step
	step[next].classList.add('on');
	// Begin Animiation
	var up = step[this.stepVal].querySelector('.up');
	var canvaschrome = step[this.stepVal].querySelector('.canvas-chrome');
	var removeClass = step[this.stepVal];




	if (window.matchMedia("(min-width: 640px)").matches) {
		removeClass.classList.remove('on');
	} else {

		TweenLite.to(up, .5, {y:-433});

		TweenLite.to(canvaschrome, .5, {
			y:200,
			onComplete: function(){ removeClass.classList.remove('on') }
		});
	}


	//canvashandler & canvas-chrome
	//transform: translate3d(0, 200px, 0);

		// Turn off old step
//		step[this.stepVal].classList.remove('on');

	// Reset App state to demo
	this.toggleState();
	// Set current step
	this.stepVal = next;
};

App.toggleState = function() {
	var state = (this.state === 'demo') ? 'instruct' : 'demo';
	this.state = state;
};

App.demo = function() {
	var demo = step[this.stepVal].querySelector('.demo');
	//	var instruct = step[this.stepVal].querySelector('.instruct');
	//	instruct.classList.remove('on')
	var instruct = step[this.stepVal].querySelector('.instruct');

	instruct.classList.add('shift');
	demo.classList.add('on');
	this.toggleState();
	// set video
	this.video = document.getElementById('video-'+(this.stepVal+1));

	// Play the video here
	this.video.play();

};

// Common gesture success callback
App.gestureSuccess = function() {
	// Disable current gesture
	this.gesture.off();

	if ('instruct' === this.state) {
		// User completed first half of step - progress to second step
		this.demo();
		// Grab the canvas element to be activated
		var element = step[this.stepVal].querySelector('#line-'+(this.stepVal+1));
		// Set the callback - this is too clever. Either run this again for another step or end the app
		var callback = (this.stepVal+1 >= step.length) ? this.end : this.gestureSuccess;
	} else {
		// Pause the video
		this.video.pause();
		// User completed entire step - progress to the next step
		this.nextStep();
		// Grab the canvas element to be activated
		var element = step[this.stepVal].querySelector('#shape-'+(this.stepVal+1));
		// Set the callback
		var callback = this.gestureSuccess;
	}

	// This may not be neccessary
	this.gesture = false;

	// Enable the new gesture;
	this.gesture = new Gesture(element, callback);
};

App.end = function() {
	// Disable current gesture
	this.gesture.off();
	// Pause the video
	this.video.pause();
	// Turn off old step
	step[this.stepVal].classList.remove('on');
	// Turn on endscreen
	var end = document.getElementById('endscreen');

	var phone = document.getElementById('endscreen-phone');
	var pen = document.getElementById('endscreen-pen');
	var heading = document.getElementById('endscreen-heading');
	var description = document.getElementById('endscreen-description');

	// Enable endscreen class
	end.classList.add('on');

	if (window.matchMedia("(min-width: 640px)").matches) {
		// Tween phone
		TweenLite.to(phone, .25, {x:60});
		// Tween pen
		TweenLite.to(pen, .25, {x:227});
		// Tween heading
		TweenLite.to(heading, .25, {x:0});
		// Tween description
		TweenLite.to(description, .25, {x:0});
	} else {
		// Tween phone
		TweenLite.to(phone, .25, {x:25});
		// Tween pen
		TweenLite.to(pen, .25, {x:294});
		// Tween heading
		TweenLite.to(heading, .25, {x:151});
		// Tween description
		TweenLite.to(description, .25, {x:28});
	}




};


App.init = function() {
	// Basic setup
	this.stepVal = 0;
	// Toogles between 'demo' and 'instruct'
	this.state = 'instruct';
	// Add on class to first step
	step[this.stepVal].classList.add('on');







	this.gesture = false;
	// Start gesture on first canvas element
	this.gesture = new Gesture(document.getElementById('shape-1'), this.gestureSuccess);
};


App.reset = function(){
	// Stop the active gesture
	this.gesture.off();
	// Remove the on class
	var resetNodes = document.querySelectorAll('.step, .demo, #endscreen');
	Array.prototype.forEach.call(resetNodes, function(node){
		node.classList.remove('on');
	}, this);
	// Re init the app
	this.init();
}

// Run it!
App.init();

console.log('end', App);

})(window.App = window.App || {});



