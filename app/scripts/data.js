function data(lang) {

	this.en = {
		canvas: {
			instruct: 'Trace Gesture Below',
			demo: 'Draw along the line to advance'
		},
		steps: [
			{
				id: 1,
				pattern: 'rectangle',
				instruct: {
					title: 'Action Memo',
					desc: 'Be ready when <br>the moment strikes.'
				},
				demo: {
					video: 'videos/video-1.webm',
					title: 'Write down your notes the<br> fast way.',
					left: 'Take down information as naturally as using a pen on paper.',
					right: 'Link your memos to actions like saving a contact.'
				}
			},
			{
				id: 2,
				pattern: 'circle',
				instruct: {
					title: 'Smart Select',
					desc: 'Experience the easy way to create interactive links.'
				},
				demo: {
					video: 'videos/video-2.webm',
					title: 'Save what interests you most with a drag of the SPen.',
					left: 'Marquee select an area and save it to your Scrapbook.',
					right: 'A screenshot will be saved with any meta data, like a URL.'
				}
			},
			{
				id: 3,
				pattern: 'triangle',
				instruct: {
					title: 'Image Clip',
					desc: 'Sometimes you find something so interesting you have to clip it.'
				},
				demo: {
					video: 'videos/video-3.webm',
					title: 'Share your finds with the ease of Image Clips.',
					left: 'Outline anything on your screen with the SPen and it’s ready to use.',
					right: 'Share your Image Clips or save them to the gallery. It’s up to you.'
				}
			},
			{
				id: 4,
				pattern: 'circle',
				instruct: {
					title: 'Screen Write',
					desc: 'Instantly give your screen a story with Screen Write and the SPen.'
				},
				demo: {
					video: 'videos/video-4.webm',
					title: 'If it’s on your screen, you can annotate it with the SPen.',
					left: 'From games to websites, take screen grabs and add notes fast.',
					right: 'Share your high-scores with friends and note reports for coworkers.'
				}
			}
		],
		endscreen: {
			title: 'Experience what else you can do',
			desc: 'or purchase the Note 4 now'
		}
	};

	this.es = {
		canvas: {
			instruct: 'Trace Gesture Below',
			demo: 'PARA AVANZAR, TOCA LA LÍNEA COMO SI DIBUJARAS'
		},
		steps: [
			{
				id: 1,
				pattern: 'rectangle',
				instruct: {
					title: 'Action Memo',
					desc: 'Prepárate para cuando llegue el momento.'
				},
				demo: {
					video: 'videos/video-1.webm',
					title: 'Escribe tus apuntes de manera rápida.',
					left: 'Escribe información de manera tan natural como si estuvieras usando papel y bolígrafo.',
					right: 'Enlaza tus apuntes a acciones, como guardar un contacto.'
				}
			},
			{
				id: 2,
				pattern: 'circle',
				instruct: {
					title: 'Smart Select',
					desc: 'Disfruta la manera fácil de crear enlaces interactivos.'
				},
				demo: {
					video: 'videos/video-2.webm',
					title: 'Guarda lo que más te interesa con sólo arrastrar el S Pen.',
					left: 'Selecciona un área y guárdala en tu Scrapbook.',
					right: 'Una captura de pantalla se guardará junto con los metadatos, tal como un URL.'
				}
			},
			{
				id: 3,
				pattern: 'triangle',
				instruct: {
					title: 'Image Clip',
					desc: 'En ocasiones encuentras cosas tan interesantes que tienes que recortarlas.'
				},
				demo: {
					video: 'videos/video-3.webm',
					title: 'Comparte fácilmente lo que encuentres con los recortes de imágenes.',
					left: 'Resalta secciones en tu pantalla con el S Pen y estarán listas al instante.',
					right: 'Comparte tus recortes o guárdalos en la galería. Tú decides.'
				}
			},
			{
				id: 4,
				pattern: 'circle',
				instruct: {
					title: 'Screen Write',
					desc: 'Escribe al instante una historia en tu toma de pantalla con Screen Write y el S Pen.'
				},
				demo: {
					video: 'videos/video-4.webm',
					title: 'Si lo tienes en tu pantalla, puedes escribirle comentarios con el S Pen.',
					left: 'Desde juegos hasta sitios web, haz tomas de pantalla y agrégales apuntes fácilmente.',
					right: 'Comparte tu puntuación con tus amigos y apuntes para tus compañeros de trabajo.'
				}
			}
		],
		endscreen: {
			title: 'Experimenta todo lo que puedes hacer',
			desc: 'O compra el Note 4 ahora.'
		}
	}

	// Output
	return (this.hasOwnProperty(lang)) ? this[lang] : false;

};
