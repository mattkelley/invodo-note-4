
function Gesture(element, callback) {
	console.log('gesture is running');

	this.callback = callback;
	this.canvas = element;
	this.pattern = this.canvas.dataset.pattern;
	this._points = new Array();
	this._r = new DollarRecognizer();
	this._g = this.canvas.getContext('2d');
	this._g.fillStyle = "rgb(0,0,225)";
	this._g.strokeStyle = "rgb(0,0,225)";
//	this._g.lineWidth = 3;
	this._rc = this.getCanvasRect(); // canvas rect on page
	this._isDown = false;

	this._g.lineWidth = 1;
	this._g.lineJoin = this._g.lineCap = 'round';

	this.Bpoints = [];


	this.off = function() {
		console.log('remove events');
		this.removeEvents();
	}

	this.addEvents();

}

Gesture.prototype.removeEvents = function() {
	var can = this.canvas;
	can.removeEventListener('touchstart', this.mouseDownEvent.bind(this));
	can.removeEventListener('touchend', this.mouseUpEvent.bind(this));
	can.removeEventListener('touchmove', this.mouseMoveEvent.bind(this));
}

Gesture.prototype.addEvents = function() {
	var can = this.canvas;
	can.addEventListener('touchstart', this.mouseDownEvent.bind(this), false);
	can.addEventListener('touchend', this.mouseUpEvent.bind(this), false);
	can.addEventListener('touchmove', this.mouseMoveEvent.bind(this), false);
}

Gesture.prototype.mouseDownEvent = function(event) {

	event.preventDefault();
	var touches = event.touches[0];
	var x = touches.clientX;
	var y = touches.clientY;


		// document.onselectstart = function() { return false; } // disable drag-select
		// document.onmousedown = function() { return false; } // disable drag-select

	this.Bpoints = [ ];

	var Xval = x -= this._rc.x;
	var Yval = y -= this._rc.y;

	this.Bpoints.push({ x: Xval, y: Yval });

	this._isDown = true;
	x -= this._rc.x;
	y -= this._rc.y - this.getScrollY();

	//console.log(x,y);

	if (this._points.length > 0) {
		this._g.clearRect(0, 0, this._rc.width, this._rc.height);
	};

	this._points.length = 1; // clear
	this._points[0] = new Point(x, y);
};

Gesture.prototype.mouseMoveEvent = function(event) {
	event.preventDefault();
	var touches = event.touches[0];
	var x = touches.clientX;
	var y = touches.clientY;




	if (this._isDown) {

		var Xval = x -= this._rc.x;
		var Yval = y -= this._rc.y;

		this.Bpoints.push({ x: Xval, y: Yval });

		x -= this._rc.x;
		y -= this._rc.y - this.getScrollY();
		this._points[this._points.length] = new Point(x, y); // append
		//this.drawConnectedPoint(this._points.length - 2, this._points.length - 1);

		this.Brush()

	}
};

Gesture.prototype.mouseUpEvent = function(event) {
	event.preventDefault();


	// document.onselectstart = function() { return true; } // enable drag-select
	// document.onmousedown = function() { return true; } // enable drag-select







	if (!this._isDown)
		return false;

	this._isDown = false;

	this._g.clearRect(0, 0, this._rc.width, this._rc.height);

	if (this._points.length <= 10) {

		return false;
	}

	var result = this._r.Recognize(this._points, true);

	if (result.Name === this.pattern || this.pattern === 'all') {
		this.callback.call(App);
		return false;
	} else {
		console.log('the gesture was ', result.Name );
	}
};

Gesture.prototype.getCanvasRect = function() {
	var w = this.canvas.width;
	var h = this.canvas.height;

	var cx = this.canvas.offsetLeft;
	var cy = this.canvas.offsetTop;

	var p = this.canvas.offsetParent;
	var all = document.getElementById('all');

	// while (p != null) {
		cx += p.offsetLeft;
		cy += p.offsetTop;

		cx += all.offsetLeft;
	//}


	return {x: cx, y: cy, width: w, height: h};
}

Gesture.prototype.getScrollY = function() {
	var scrollY = 0;
	if (typeof(document.body.parentElement) != 'undefined') {
		scrollY = document.body.parentElement.scrollTop; // IE
	} else if (typeof(window.pageYOffset) != 'undefined') {
		scrollY = window.pageYOffset; // FF
	}

	//return { left: this.canvas.offsetLeft, top: this.canvas.offsetTop };

	return scrollY;
}

Gesture.prototype.drawConnectedPoint = function(from, to) {
	// this._g.beginPath();
	// this._g.moveTo(this._points[from].X, this._points[from].Y);
	// this._g.lineTo(this._points[to].X, this._points[to].Y);
	// this._g.closePath();
	// this._g.stroke();
}

Gesture.prototype.Brush = function() {

	this._g.beginPath();

	this._g.moveTo(this.Bpoints[this.Bpoints.length - 2].x, this.Bpoints[this.Bpoints.length - 2].y);
	this._g.lineTo(this.Bpoints[this.Bpoints.length - 1].x, this.Bpoints[this.Bpoints.length - 1].y);
	this._g.stroke();

	for (var i = 0, len = this.Bpoints.length; i < len; i++) {
		dx = this.Bpoints[i].x - this.Bpoints[this.Bpoints.length-1].x;
		dy = this.Bpoints[i].y - this.Bpoints[this.Bpoints.length-1].y;
		d = dx * dx + dy * dy;

		if (d < 1000) {
			this._g.beginPath();
			this._g.strokeStyle = 'rgba(0,0,0,0.3)';
			this._g.moveTo( this.Bpoints[this.Bpoints.length-1].x + (dx * 0.2), this.Bpoints[this.Bpoints.length-1].y + (dy * 0.2));
			this._g.lineTo( this.Bpoints[i].x - (dx * 0.2), this.Bpoints[i].y - (dy * 0.2));
			this._g.stroke();
		}
	}

}
